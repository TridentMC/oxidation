oxidation
=========

This is **oxidation**, a fully decoupled bittorrent client.

oxidation, on its own, is only a server - the primary network component. It does most of the heavy lifting for torrents, manages their status and allows for steady reporting. A client is needed to interface with the server, the most simple of which is `oxi`.
