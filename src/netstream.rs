/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

//! This is a module to encapsulate some of the dual prod/testing network
//! stream logic necessary to keep this server mockable and testable.
//!
//! It utilizes the `mockstream` crate in order to create an `enum` (as per
//! the crate's reccomendation) that encapsulates the two types of network
//! logic that will be handled by the application:
//!		* Testing (Mocked)
//! 	* TCP (Tcp)

extern crate bufstream;
extern crate mockstream;

use mockstream::SharedMockStream;
use bufstream::BufStream;
use std::io;
use std::net::TcpStream;

#[allow(dead_code)]
/// `NetStream` is the primary enum behind the logic of this module,
/// encapsulating both forms of stream logic to be handled invidiually.
pub enum NetStream {
    /// A 'mocked' stream (used only to test application logic).
    Mocked(SharedMockStream),
    /// A TCP stream, able to handle two-way TCP communication.
    Tcp(BufStream<TcpStream>),
}

impl io::Read for NetStream {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        match *self {
            NetStream::Mocked(ref mut s) => s.read(buf),
            NetStream::Tcp(ref mut s) => s.read(buf),
        }
    }
}

impl io::Write for NetStream {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        match *self {
            NetStream::Mocked(ref mut s) => s.write(buf),
            NetStream::Tcp(ref mut s) => s.write(buf),
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        match *self {
            NetStream::Mocked(ref mut s) => s.flush(),
            NetStream::Tcp(ref mut s) => s.flush(),
        }
    }
}
