/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

//! Oxidation is a headless torrent client, which can be interfaced with
//! through multiple clients utilizing a uniform command set and structure.
//!
//! Primarily, this is just a server that should run in the background, or
//! on something relatively dedicated. The intention of its operation is
//! something similar to X11's server/client design, wherin the base set
//! of mechanical operations is defined and able to be utilized uniformly,
//! with enough variety and customizability to allow clients to access
//! and utilize information in the way of their choosing.

#![cfg_attr(feature = "cargo-clippy", deny(pedantic))]

extern crate bufstream;
extern crate json;
#[macro_use]
extern crate log;
extern crate mockstream;
extern crate oxidant;

use std::io::Write;
use std::thread;
use std::error::Error;
use std::net::TcpListener;
use bufstream::BufStream;
use std::io::BufRead;

pub mod netstream;
pub mod command;

use netstream::NetStream;

/// Initializes most of the application logic, including the basic task of
/// setting up our `TcpListener`
///
/// # Errors
///
/// This function returns an error if:
/// 	* The server is unable to be bound to the requested host and port
///		* There's a problem evaluating the stream from the `TcpListener`
pub fn run() -> Result<(), String> {
    info!("run: spawning tcp server on 127.0.0.1:4422");
    let listener = match TcpListener::bind("127.0.0.1:4422") {
        Ok(l) => l,
        Err(e) => {
            return Err(e.description().to_string());
        }
    };

    for stream in listener.incoming() {
        let stream = match stream {
            Ok(s) => s,
            Err(e) => {
                error!("run: stream error: {}", e);
                continue;
            }
        };

        info!("run: recieved connection - spawning...");

        thread::spawn(|| {
            let mut strm = NetStream::Tcp(BufStream::new(stream));
            handle_connection(&mut strm);
        });
    }

    Ok(())
}

/// Handles a connection once its been successfully evaluated.
///
/// This is the primary function for the thread spawned on the receipt of a
/// valid `NetStream` of some type. Most of the other functions are evaluated
/// within the context of this function.
///
/// # Arguments
///
/// `stream` - A valid `NetStream`, able to be read and written to for
/// command receipt and information distribution.
fn handle_connection(stream: &mut NetStream) {
    info!("handle_connection: connection established - listening for command");

    let mut blob = String::new();
    match *stream {
        NetStream::Mocked(_) => {
            blob = String::from("{\"command\": \"test\"}");
        }
        NetStream::Tcp(ref mut s) => match s.read_line(&mut blob) {
            Ok(_) => {}
            Err(e) => {
                error!("handle_connection: problem reading stream input to string: {}", e);
                return;
            }
        },
    }

    debug!("handle_connection: command recieved - attempting to parse...");

    let command = match oxidant::Command::deserialize(blob.as_ref()) {
        Ok(c) => c,
        Err(e) =>  {
            error!("handle_connection: problem deserializing command: {}", e);
            return;
        }
    };

    debug!("handle_connection: command found - matching known commands...");

    process_command(stream, command);
}

/// Outputs an error to the main logger, and additionally _attempts_ to tell
/// the client something has gone wrong.
///
/// # Arguments
///
/// `stream` - A `NetStream` to write the error data to.
/// `err` - The error message to output to the log, and to the stream.
fn throw_error(stream: &mut NetStream, err: &str) {
    error!("error thrown: {}", err);
    let err = format!("{{\"error\": \"{}\"}}\n", err);
    match stream.write(err.as_bytes()) {
        Ok(_) => {}
        Err(e) => {
            error!(
                "problem writing to stream for error - something is very wrong: {}",
                e
            );
        }
    }
}

/// Parses out command data, for the express purpose of handling
/// the logic in `process_command`
///
/// # Arguments
///
/// `blob` - The recieved text from the client, (hopefully) in JSON format.
///
/// # Errors
///
/// This function returns an error if:
/// 	* The JSON parsing fails for any reason
///		* The parsed JSON object doesn't have the `command` key.
///		* The command key doesn't have valid string data
///
/// # Returns
///
/// This function returns, on success, a `Result::Ok` containing a `String`
/// with the parsed command.


/// Processes a command from its constituent parts, and based upon the current
/// state of the server, then writes this information to the stream.
///
/// # Arguments
///
/// `stream` - A valid `NetStream` to write to.
/// `command` - The command name extracted from the input.
fn process_command(stream: &mut NetStream, command: oxidant::Command) {
    let response = match command {
        oxidant::Command::Test => command::test_handler(),
        oxidant::Command::HealthCheck => command::health_check_handler(),
        oxidant::Command::Echo(ref s) => command::echo_handler(s),
        oxidant::Command::Add(a, b) => command::add_handler(a, b),
        _ => {
            throw_error(stream, &format!("process_command: unknown command {:?}", command));
            return;
        }
    };
    
    match response {
        Ok(b) => {
            debug!("process_command: writing response '{}'", b.trim());
            match stream.write(b.as_bytes()) {
                Ok(_) => {}
                Err(e) => throw_error(stream, e.description()),
            };
        },
        Err(e) => {
            throw_error(stream, &format!("process_command: error with command {:?} - {}", command, e));
        }
    }

    debug!("process_command: response should have been written");
}

#[cfg(test)]
mod tests {
    use super::*;
    use mockstream::SharedMockStream;

    #[test]
    fn test_connection_response() {
        let mut mock = SharedMockStream::new();
        let mut conn = NetStream::Mocked(mock.clone());
        handle_connection(&mut conn);

        let vres = mock.pop_bytes_written();
        let res = match std::str::from_utf8(&vres) {
            Ok(v) => v,
            Err(e) => panic!("Invalid UTF-8 recieved: {}", e),
        };

        assert_eq!("{\"text\": \"hello, world!\"}\n", res);
    }
}
