//! Container for all command handlers and command-related functions that oxidation might need to access.

pub fn test_handler() -> Result<String, String> {
    Ok("{\"text\": \"hello, world!\"}\n".to_string())
}

pub fn health_check_handler() -> Result<String, String> {
    Ok("{\"status\": \"ok\"}\n".to_string())
}

pub fn echo_handler(e: &str) -> Result<String, String> {
    if e.is_empty() {
        Err("no valid echo parameter found".to_string())
    } else {
        Ok(format!("{{\"echo\": \"{}\"}}", e))
    }
}

pub fn add_handler(a: i32, b: i32) -> Result<String, String> {
    Ok(format!("{{\"result\": {}}}", a+b))
}