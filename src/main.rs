/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

//! Oxidation is a headless torrent client, which can be interfaced with
//! through multiple clients utilizing a uniform command set and structure.
//!
//! Primarily, this is just a server that should run in the background, or
//! on something relatively dedicated. The intention of its operation is
//! something similar to X11's server/client design, wherin the base set
//! of mechanical operations is defined and able to be utilized uniformly,
//! with enough variety and customizability to allow clients to access
//! and utilize information in the way of their choosing.

#[macro_use]
extern crate log;
extern crate log4rs;
extern crate oxidation_server;

fn main() {
    let config_file = "config/log4rs.yaml";
    match log4rs::init_file("config/log4rs.yaml", Default::default()) {
        Ok(_) => {}
        Err(e) => {
            eprintln!("unable to open config file at '{}' - {}", config_file, e);
            return;
        }
    }

    info!("booting up oxidation...");

    std::process::exit(match oxidation_server::run() {
        Ok(_) => 0,
        Err(e) => {
            error!("oxidation failed unexpectedly: {}", e);
            1
        }
    });
}
